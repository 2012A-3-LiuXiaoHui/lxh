package com.bbyb.operating.examination.model.po;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 19:04
 * @Version v1.0
 */
@Data
public class LoginForm {

    @NotBlank(message = "邮箱不能为空")
    private String email;

    @NotBlank(message = "密码不能为空")
    @Length(min = 6,message = "密码长度最小为6位")
    private String password;

    @NotBlank
    @Pattern(regexp = "^A-\\d{12}-\\d{4}$",message = "格式错误")
    private String other;

}