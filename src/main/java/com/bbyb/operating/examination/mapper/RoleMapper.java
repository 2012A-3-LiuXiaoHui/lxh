package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.Role;
import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role row);

    Role selectByPrimaryKey(Integer id);

    List<Role> selectAll();

    int updateByPrimaryKey(Role row);

    Role findByRoleCode(String roleCode);
}