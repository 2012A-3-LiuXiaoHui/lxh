package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationMajor;
import java.util.List;

public interface ExaminationMajorMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationMajor row);

    ExaminationMajor selectByPrimaryKey(Integer id);

    List<ExaminationMajor> selectAll();

    int updateByPrimaryKey(ExaminationMajor row);
}