package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleService;
import com.bbyb.operating.examination.service.RoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 16:19
 * @Version v1.0
 */
@RestController()
@RequestMapping("/RoleUser")
public class RoleUserController {
    @Autowired
    RoleUserService roleUserService;
    @PostMapping("selectAll")
    public CommonResult<List<RoleUser>> selectAll(){
        return roleUserService.selectAll();
    }
    @PostMapping(value = "/insertRole")
    public CommonResult insert(@RequestBody RoleUser roleUser){
        return roleUserService.insert(roleUser);
    }
    @PostMapping("deleteByPrimaryKey")
    public CommonResult deleteByPrimaryKey(@RequestParam Integer id){
        return roleUserService.deleteByPrimaryKey(id);
    }
    @PostMapping("updateByPrimaryKey")
    public CommonResult updateByPrimaryKey(@RequestBody RoleUser roleUser){
        return roleUserService.updateByPrimaryKey(roleUser);
    }
    @PostMapping("selectByPrimaryKey")
    public CommonResult<RoleUser> selectByPrimaryKey(Integer id){
        return roleUserService.selectByPrimaryKey(id);
    }
    @GetMapping("/test")
    public void test() {
        throw new RuntimeException("手动触发的异常");
    }
}
