package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.LoginForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 19:06
 * @Version v1.0
 */

@Slf4j
@RestController
public class LoginController {

    @PostMapping("/login")
    public void login(@Valid LoginForm loginForm, BindingResult bindingResult) {
        log.info("loginForm:{}", loginForm);
        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                log.info("error:{}", error.getDefaultMessage());
            }
        } else {
            log.info("参数校验成功！");
        }
    }
}