package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 16:19
 * @Version v1.0
 */
@RestController()
@RequestMapping("/role")
public class RoleController {
    @Autowired
    RoleService roleService;
    @PostMapping("selectAll")
    public CommonResult<List<Role>> selectAll(){
        return roleService.selectAll();
    }
    @PostMapping(value = "/insertRole")
    public CommonResult insertRole(@RequestBody Role role){
        return roleService.insertRole(role);
    }
    @PostMapping("deleteByPrimaryKey")
    public CommonResult deleteByPrimaryKey(@RequestParam Integer id){
        return roleService.deleteByPrimaryKey(id);
    }
    @PostMapping("updateByPrimaryKey")
    public CommonResult updateByPrimaryKey(@RequestBody Role role){
        return roleService.updateByPrimaryKey(role);
    }
    @PostMapping("selectByPrimaryKey")
    public CommonResult<Role> selectByPrimaryKey(Integer id){
        return roleService.selectByPrimaryKey(id);
    }
    @GetMapping("/test")
    public void test() {
        throw new RuntimeException("手动触发的异常");
    }
}
