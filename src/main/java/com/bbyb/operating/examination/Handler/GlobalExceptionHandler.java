package com.bbyb.operating.examination.Handler;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 19:16
 * @Version v1.0
 */

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception ex) {
        // 处理异常逻辑
        // 返回自定义的错误信息或者其他相应的处理
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("发生了未知错误");
    }
}
