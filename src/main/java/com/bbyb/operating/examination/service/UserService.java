package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;

import java.util.List;

public interface UserService {
    String addUser(User user);

    List<User> getAllUser();

    List<User> selectAll();

    CommonResult InsertUser(User user);

    CommonResult deleteByPrimaryKey(Integer id);


}
