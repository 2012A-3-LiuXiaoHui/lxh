package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.vo.CommonResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 16:20
 * @Version v1.0
 */
public interface RoleService {
    CommonResult insertRole(Role role);


    CommonResult deleteByPrimaryKey(Integer id);

    CommonResult updateByPrimaryKey(@Param("role") Role role);

    CommonResult<Role> selectByPrimaryKey(Integer id);

    CommonResult<List<Role>> selectAll();
}
