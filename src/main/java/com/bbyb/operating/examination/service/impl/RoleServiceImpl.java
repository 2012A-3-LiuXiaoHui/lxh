package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.RoleMapper;
import com.bbyb.operating.examination.mapper.RoleUserMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 16:20
 * @Version v1.0
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    RoleUserMapper roleUserMapper;

    @Override
    public CommonResult insertRole(Role role) {
        if (role.getRoleCode() != null) {
            String roleCode = role.getRoleCode();
            Role byRoleCode = roleMapper.findByRoleCode(roleCode);
            if (byRoleCode != null) {
                return new CommonResult(500, "角色编码不能重复");
            }
            int i = roleMapper.insert(role);
            return new CommonResult(200, "成功", true, null);
        }
        return new CommonResult(500, "角色编码必填", true, null);
    }

    @Override
    public CommonResult deleteByPrimaryKey(Integer id) {
        if (id == null) {
            throw new RuntimeException();
        }
        roleUserMapper.deleteByRole(id);
        int i = roleMapper.deleteByPrimaryKey(id);
        return new CommonResult(200, "删除成功");
    }

    @Override
    public CommonResult updateByPrimaryKey(Role role) {
        roleMapper.updateByPrimaryKey(role);
        return new CommonResult(200, "修改成功");
    }

    @Override
    public CommonResult<Role> selectByPrimaryKey(Integer id) {
        Role role = roleMapper.selectByPrimaryKey(id);
        return new CommonResult(200, "查询成功", true, role);
    }

    @Override
    public CommonResult<List<Role>> selectAll() {
        List<Role> roles = roleMapper.selectAll();
        return new CommonResult(200, "查询成功", true, roles);
    }
}