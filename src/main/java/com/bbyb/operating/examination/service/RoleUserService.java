package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.vo.CommonResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 16:20
 * @Version v1.0
 */
public interface RoleUserService {
    CommonResult insert(RoleUser roleUser);


    CommonResult deleteByPrimaryKey(Integer id);

    CommonResult updateByPrimaryKey(@Param("RoleUser") RoleUser RoleUser);

    CommonResult<RoleUser> selectByPrimaryKey(Integer id);

    CommonResult<List<RoleUser>> selectAll();
}
