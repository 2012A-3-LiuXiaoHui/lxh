package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.UserMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账号服务
 * className: UserServiceImpl
 * datetime: 2023/2/10 14:29
 * author: lx
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public String addUser(User user) {
        if(userMapper.InsertUser(user) == 1){
            return null;
        }
        return "保存用户信息失败";
    }

    @Override
    public List<User> getAllUser() {
        return userMapper.selectAll();
    }

    @Override
    public List<User> selectAll() {
        List<User> users = userMapper.selectAll();
        return users;
    }

    @Override
    public CommonResult InsertUser(User user) {
        if(user.getUserCode()!=null){
            String userCode = user.getUserCode();
            User byUserCode = userMapper.findByUserCode(userCode);
            if(byUserCode!=null){
                return new CommonResult(500,"角色编码不能重复");
            }
            int i = userMapper.InsertUser(user);
            return new CommonResult(200,"成功",true,null);
        }
        return new CommonResult(500,"角色编码必填",true,null);

    }

    @Override
    public CommonResult deleteByPrimaryKey(Integer id) {
        int i = userMapper.deleteByPrimaryKey(id);
        return new CommonResult(200,"删除成功");
    }



}
