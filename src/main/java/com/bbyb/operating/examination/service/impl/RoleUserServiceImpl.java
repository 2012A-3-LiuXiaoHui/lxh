package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.RoleMapper;
import com.bbyb.operating.examination.mapper.RoleUserMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleService;
import com.bbyb.operating.examination.service.RoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author 26704
 * @LastChangeDate 2023/8/10 16:20
 * @Version v1.0
 */
@Service
public class RoleUserServiceImpl implements RoleUserService {
    @Autowired
    RoleUserMapper roleUserMapper;

    @Override
    public CommonResult insert(RoleUser roleUser) {
        roleUserMapper.insert(roleUser);
        return new CommonResult(200, "添加成功", true, null);
    }

    @Override
    public CommonResult deleteByPrimaryKey(Integer id) {
        if (id == null) {
            throw new RuntimeException();
        }
        int i = roleUserMapper.deleteByPrimaryKey(id);
        return new CommonResult(200, "删除成功");
    }

    @Override
    public CommonResult updateByPrimaryKey(RoleUser roleUser) {
        roleUserMapper.updateByPrimaryKey(roleUser);
        return new CommonResult(200, "修改成功");
    }

    @Override
    public CommonResult<RoleUser> selectByPrimaryKey(Integer id) {
        RoleUser roleUser = roleUserMapper.selectByPrimaryKey(id);
        return new CommonResult(200, "查询成功", true, roleUser);
    }

    @Override
    public CommonResult<List<RoleUser>> selectAll() {
        List<RoleUser> roleUsers = roleUserMapper.selectAll();
        return new CommonResult(200, "查询成功", true, roleUsers);
    }
}